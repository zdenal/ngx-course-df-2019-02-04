import { environment } from './../environments/environment';
import { AppConfig } from './app.model';
import { CarsConfig } from './cars/cars.model';

export const appConfigValue: AppConfig = {
  version: '0.0.0'
};

export const carsConfigValue: CarsConfig = {
  getCarsUrl: `${environment.firebaseUriPrefix}/cars.json`,
  createCarUrl: `${environment.firebaseUriPrefix}/cars.json`,
  getCarUrl: `${environment.firebaseUriPrefix}/cars/#{id}.json`,
  updateCarUrl: `${environment.firebaseUriPrefix}/cars/#{id}.json`,
  deleteCarUrl: `${environment.firebaseUriPrefix}/cars/#{id}.json`
};
