export interface Environment {
  production: boolean;
  firebaseUriPrefix: string;
}
